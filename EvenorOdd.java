//A Program to check even or odd
import java.util.*;
public class Main
{
	public static void main(String[] args) {
		Scanner obj = new Scanner(System.in);
		//Taking input from user
		System.out.println("Enter a Number");
		int Number = obj.nextInt();
		if(Number%2!=0)
		System.out.println(Number+" "+"is odd number");
		else
		System.out.println(Number+" "+"is even number");
		
	}
}
