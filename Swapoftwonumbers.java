//A Program to check swap of two numbers
import java.util.*;
public class Main
{
	public static void main(String[] args) {
		Scanner obj = new Scanner(System.in);
		//Taking input from user
		System.out.println("Enter a Number1");
		int Number1 = obj.nextInt();
		System.out.println("Enter a Number2");
		int Number2 = obj.nextInt();
		System.out.println("Numbers before Swap are :"+Number1+" "+Number2);
		//Logic
		Number1 = Number1 + Number2;
		Number2 = Number1 - Number2;
		Number1 = Number1 - Number2;
		System.out.println("Numbers after Swap are :"+Number1 +" "+Number2);
	
	}
}
