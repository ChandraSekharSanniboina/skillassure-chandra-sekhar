// A Program to accept principle,rate of interest and time.Calculate Simple interest
import java.util.*;
public class Main
{
	public static void main(String[] args) {
		Scanner obj = new Scanner(System.in);
		//Taking input from user
		System.out.println("Enter principle");
		double Principle = obj.nextDouble();
		System.out.println("Enter Rate of interest");
		double Rateofinterest = obj.nextDouble();
		System.out.println("Enter Time");
		double Time = obj.nextDouble();
		//Logic
		double Simpleinterest = ((Principle*Rateofinterest*Time)/100);
		//Displaying the result
		System.out.println("Simple interest is :"+Simpleinterest);
		
	}
}
