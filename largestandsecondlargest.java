//A Program to check largest and second largest
import java.util.*;
public class Main
{
	public static void main(String[] args) {
		Scanner obj = new Scanner(System.in);
		//Taking input from user
		System.out.println("Enter a Number1");
		int Number1 = obj.nextInt();
		System.out.println("Enter a Number2");
		int Number2 = obj.nextInt();
		System.out.println("Enter a Number3");
		int Number3 = obj.nextInt();
		//Logic for largest
		if(Number1>Number2 && Number1>Number3)
		System.out.println("The largest number is "+Number1);
		else if(Number2>Number3)
		System.out.println("The largest number is "+Number2);
		else 
		System.out.println("The largest number is "+Number3);
		
		//Logic for second largest
		if (Number1 > Number2 && Number1 < Number3 || Number1 > Number3 && Number1 < Number2)
            System.out.println("The second largest number is "+Number1);
        else if (Number2 > Number1 && Number2 < Number3 || Number2 > Number3 && Number2 < Number1)
            System.out.println("The second largest number is "+Number2);
        else
            System.out.println("The second largest number is "+Number3);
	
	}
}
